
Add the ansible user to allow ssh access.

`adduser --shell /bin/bash --gecos "" ansible`

Run `visudo`
And add the following line:
`ansible ALL=(ALL) NOPASSWD:ALL`

Copy the id.pub to the client:
`ssh-copy-id ansible@192.168.x.x`

Test remote login and sudo

Disable console login for the user:
`usermod -L ansible`


Use ansible

`ansible-playbook /path/to/playbook.yml -i /path/to/inventory/file (add -l 'subset' to only connect to a subset)`
